from os import sched_getaffinity
from types import MappingProxyType

__all__ = [
    'CPU_QUANTITY',
    'GUNICORN_DEFAULTS',
    'GUNICORN_TYPE_MAP',
    'GUNICORN_SETTINGS_FALSY_STRINGS',
    'GUNICORN_INI_CONFIG_FILEPATH_PREFIX',
    'GUNICORN_DEFAULT_CONFIG_SECTION',
    'GUNICORN_INI_CONFIG_ENV_KEY',
]


# os.cpu_count() takes wrong results on various lxc-like installations.
CPU_QUANTITY = len(sched_getaffinity(0))


GUNICORN_DEFAULTS = MappingProxyType({
    'host': '0.0.0.0',
    'port': '8080',
    'bind': None,
    'timeout': 300,
    'keepalive': 2,
    'worker_class': 'sync',
    'workers': CPU_QUANTITY,
    'worker_connections': 100,
    'daemon': False,
    'pidfile': None,
    'user': None,
    'group': None,
    'umask': '0',
    'log_config': None,
    'reload': False
})


# Gunicorn config type map for params.
GUNICORN_TYPE_MAP = MappingProxyType({
    'str': {
        'worker_class',
        'workers',
        'user',
        'pidfile',
        'group',
        'umask',
    },
    'int': {
        'worker_connections',
        'timeout',
        'keepalive',
    },
    'bool': {
        'daemon',
        'reload',
    },
    # Special type of parser.
    'bind': {
        'bind',
    }
})

GUNICORN_SETTINGS_FALSY_STRINGS = frozenset({
    'false', '0', 'no', 'null', 'none'
})

GUNICORN_INI_CONFIG_FILEPATH_PREFIX = 'ini:'

GUNICORN_DEFAULT_CONFIG_SECTION = 'gunicorn'

GUNICORN_INI_CONFIG_ENV_KEY = 'GUNICORN_INI_CONFIG_FILEPATH'
