from configparser import ConfigParser
from genericpath import exists
from logging import captureWarnings
from logging.config import fileConfig
from os import R_OK, access
from os.path import abspath, dirname

from .env import get_ini_filepath_from_env

__all__ = [
    'parse_settings_from_file',
    'parse_settings_from_env',
]


def parse_settings_from_file(ini_filepath: str,
                             section: str,
                             init_logging: bool = False) -> dict:
    """
    Parsing settings from ini configuration file.

    :param ini_filepath: Configuration filepath.
    :param section: Section name to parse.
    :param init_logging: Logging initialization flag.
    :return:
    """
    ini_filepath = abspath(ini_filepath)

    if not exists(ini_filepath) or not access(ini_filepath, R_OK):
        raise FileNotFoundError(
            'File {!r} does not exist or not readable.'.format(ini_filepath)
        )

    ini_dirpath = dirname(ini_filepath)
    defaults = {'here': ini_dirpath}

    if init_logging:
        fileConfig(
            ini_filepath, defaults=defaults, disable_existing_loggers=False
        )
        captureWarnings(True)

    parser = ConfigParser(defaults=defaults)
    parser.read(ini_filepath)

    sections = parser.sections()

    if section not in sections:
        raise KeyError('There is not section {!r} in configuration file '
                       '{!r}.'.format(section, ini_filepath))

    settings = dict(parser.items(section))
    settings['__file__'] = ini_filepath
    return settings


def parse_settings_from_env(section: str,
                            init_logging: bool = False) -> dict:
    return parse_settings_from_file(
        get_ini_filepath_from_env(),
        section,
        init_logging=init_logging
    )
