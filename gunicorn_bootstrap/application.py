from argparse import ArgumentParser, Namespace
from os import environ
from typing import Any, Dict, Optional, Sequence

from gunicorn.app.wsgiapp import WSGIApplication
from gunicorn.config import Setting, validate_string
from gunicorn.errors import ConfigError

from .constant import GUNICORN_DEFAULT_CONFIG_SECTION, \
    GUNICORN_INI_CONFIG_ENV_KEY, GUNICORN_INI_CONFIG_FILEPATH_PREFIX
from .settings import GunicornSettings

__all__ = [
    'GunicornWSGIApplication',
]


def _get_config_from_ini(filepath: str) -> Dict[str, Any]:
    """Getting options from config url"""
    return GunicornSettings.from_file(
        filepath,
        GUNICORN_DEFAULT_CONFIG_SECTION,
        # todo: Make desicion of logging nessesity by cli options.
        init_logging=True,
    ).as_dict()


# It is created like ``gunicorn.config.ConfigFile`` setting.
# Every settings is processed by gunicorn automatically, we don`t hae to add
# it anywhere.
class IniConfigFile(Setting):
    name = 'ini'
    section = 'Ini Config File'
    cli = ['-i', '--ini']
    meta = "INI_FILEPATH"
    validator = validate_string
    default = None
    desc = 'The Gunicorn config INI file.'


class GunicornWSGIApplication(WSGIApplication):
    """Gunicorn application with ini config filepath ability."""
    def __init__(self,
                 app_uri: str,
                 usage: Optional[str] = None,
                 prog: Optional[str] = None):
        self.app_uri = app_uri
        super().__init__(usage=usage, prog=prog)

    def init(self,
             parser: ArgumentParser,
             opts: Namespace,
             args: Sequence[str]) -> Optional[Dict[str, Any]]:
        ini_config_filepath = opts.ini

        if opts.paste or not ini_config_filepath:
            return super().init(parser, opts, args)

        if opts.config:
            raise ConfigError('"--config" and "--ini" options can not be '
                              'defined together.')

        environ[GUNICORN_INI_CONFIG_ENV_KEY] = ini_config_filepath
        self.cfg.set("default_proc_name", self.app_uri)
        # We mutate opts Namespace. It is not good, but we don`t want copy
        # whole "load_config" method.
        opts.config = '{}{}'.format(
            GUNICORN_INI_CONFIG_FILEPATH_PREFIX, ini_config_filepath,
        )

    def get_config_from_filename(self, filename: str) -> Dict[str, Any]:
        if filename.startswith(GUNICORN_INI_CONFIG_FILEPATH_PREFIX):
            return _get_config_from_ini(
                filename[len(GUNICORN_INI_CONFIG_FILEPATH_PREFIX):]
            )

        return super().get_config_from_filename(filename)
