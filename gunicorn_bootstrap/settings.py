import sys
from typing import Any, Callable, Iterable, Iterator, Mapping, Optional, Set, \
    Tuple

from .constant import GUNICORN_DEFAULTS, GUNICORN_SETTINGS_FALSY_STRINGS, \
    GUNICORN_TYPE_MAP
from .env import get_ini_filepath_from_env
from .parser import parse_settings_from_file

__all__ = [
    'GunicornSettings',
]


class GunicornSettings(Iterable):
    """Gunicorn settings container."""
    __slots__ = '_settings', '_defaults', '_type_map'

    def __init__(self,
                 settings: Mapping[str, str],
                 defaults: Optional[Mapping[str, Any]] = None,
                 type_map: Optional[Mapping[str, Set]] = None) -> None:
        self._settings = settings

        if defaults is None:
            defaults = GUNICORN_DEFAULTS

        self._defaults = dict(defaults)

        if type_map is None:
            type_map = GUNICORN_TYPE_MAP

        self._type_map = type_map

    def _get_parser(self, type_: str) -> Callable[[str], Any]:
        return getattr(self, '_get_{}'.format(type_))

    def _get_str(self, key: str) -> str:
        settings = self._settings

        if key in settings:
            return settings[key]

        return self._defaults[key]

    def _get_int(self, key: str) -> int:
        try:
            return int(self._settings.get(key))

        except (TypeError, ValueError):
            return self._defaults[key]

    def _get_bool(self, key: str) -> bool:
        settings = self._settings

        if key in settings:
            return settings[
                key
            ].lower().strip() not in GUNICORN_SETTINGS_FALSY_STRINGS

        return self._defaults[key]

    def _get_bind(self, key: str) -> str:
        self._defaults.setdefault('bind', None)
        return self._get_str(key) or ':'.join(
            map(self._get_str, ('host', 'port'))
        )

    def __iter__(self) -> Iterator[Tuple[str, Any]]:
        for type_, type_keys in self._type_map.items():

            parser = self._get_parser(type_)

            for key in type_keys:
                yield key, parser(key)

    def as_dict(self) -> dict:
        return dict(self)

    def as_tuple(self) -> tuple:
        return tuple(self)

    def to_module(self, module_name: str) -> None:
        """Add local variables to module, which had been passed on as
        # py-settings module."""
        module_ = sys.modules[module_name]

        for key, value in self:
            setattr(module_, key, value)

    @classmethod
    def from_file(cls,
                  ini_filepath: str,
                  section: str,
                  init_logging: bool = False,
                  **kwargs) -> 'GunicornSettings':
        return cls(
            parse_settings_from_file(
                ini_filepath,
                section,
                init_logging=init_logging,
            ),
            **kwargs,
        )

    @classmethod
    def from_env(cls, *args, **kwargs) -> 'GunicornSettings':
        """Get settings from env."""
        return cls.from_file(
            get_ini_filepath_from_env(),
            *args,
            **kwargs,
        )
