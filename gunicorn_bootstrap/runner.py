from .application import GunicornWSGIApplication

__all__ = [
    'run_gunicorn',
]


def run_gunicorn(app_name: str,
                 app_uri: str) -> None:
    """Guncorn command parser."""
    GunicornWSGIApplication(
        app_uri,
        usage='%(prog)s [OPTIONS] [APP_MODULE]',
        prog='App {!r} gunicorn runner.'.format(app_name),
    ).run()
