from os import environ

from .constant import GUNICORN_INI_CONFIG_ENV_KEY

__all__ = [
    'get_ini_filepath_from_env',
]


def get_ini_filepath_from_env() -> str:
    if GUNICORN_INI_CONFIG_ENV_KEY not in environ:
        raise KeyError('Ini config filepath is not defined.')

    return environ[GUNICORN_INI_CONFIG_ENV_KEY]

