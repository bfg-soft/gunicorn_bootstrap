from setuptools import find_packages, setup

setup(
    name='gunicorn_bootstrap',
    version='0.2.0',
    packages=find_packages(),
    url='https://bitbucket.org/bfg-soft/gunicorn_bootstrap',
    license='MIT',
    author='BFG-Soft LLC',
    author_email='info@bfg-soft.ru',
    description='Tools for gunicorn web server with ini config startup.',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development',
    ],
    install_requires=[
        'gunicorn<21.0.0',
    ],
)
