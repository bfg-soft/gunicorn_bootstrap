# Gunicorn bootstrap tool.

A tool for [gunicorn](https://gunicorn.org/) starting up with non-language 
config file.
 

## Getting started

### Installation

We have no pypi account yet and we have no private pypi repository.
So, you should install this package as usual with `pip` package manager in 
virtualenv or just globally from source:
```bash
pip install git+https://bitbucket.org/bfg-soft/gunicorn_bootstrap.git
```
If you want to install specific version or branch, you should execute:
```bash
pip install git+https://bitbucket.org/bfg-soft/gunicorn_bootstrap.git@v0.1.0
```


### Usage
Package provides function `run_gunicorn`, that can be used to starting up 
gunicorn in your project, like this.

```python
from functools import partial

from gunicorn_bootstrap import run_gunicorn

__all__ = [
    'run_web_server',
]

run_web_server = partial(
    run_gunicorn,
    'your_project_name',
    'python:your_project.gunicorn_settings',
    'your_project.app:APP',
)


if __name__ == '__main__':
    run_web_server()
```

where:

* `your_project_name` is just a project label, that appears in command 
line help message on this script.
* `your_project` - package name of your project.
* `your_project.app:APP` - created WSGI app variable in your package module 
`your_project.app`.
* `your_project.gunicorn_settings` - language config file (python module)
 of gunicorn settings. This file configures automatically based on 
 non-language config file.

 
Example of `your_project.gunicorn_settings`:
```python
from gunicorn_bootstrap import GunicornSettings

__all__ = []


GunicornSettings.from_env(
    'gunicorn'  # Ini config file section name for gunicorn settings.
).to_module(__name__)
```

***Attention**: `gunicorn_bootstrap` currently supported only ini config files.*


After that, you can use this script from console, help info available with 
flag `--help`. 

We recommend add it to `entry_points['console_scripts']` of `setup.py` file in 
your application.

Available script options:

* `-i` or `--ini` - path to config file. Required.
* `-g` or `--gunicorn` - path to gunicorn binary file. Optionally, it points by 
default to `gunicorn` file in directory, where `python` interpreter binary 
file is situated.
* `-w` or `--worker-quantity` - quantity of workers of gunicorn, that must be
 started. Optionally, it equals by default to quantity of available CPU.


## License

This project is licensed under the MIT License - see the 
[LICENSE.txt](LICENSE.txt) file for details.
